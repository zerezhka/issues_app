package com.example.issue_app.ui.create

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import com.example.issue_app.R
import com.example.issue_app.data.issue.Priority
import com.example.issue_app.repository.DATE_FORMAT
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.repository.parseDate
import com.example.issue_app.ui.EXTRA_KEY_OF_FRAGMENT
import com.example.issue_app.ui.priority.SpinnerAdapter
import com.example.issue_app.utils.DialogHelper
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

const val ISSUE_ID = "issue_id"
const val NEED_TO_SHOW_CREATE_FRAGMENT = "CreateFragment"

class CreateFragment : Fragment() {

    companion object {
        fun newInstance(): CreateFragment {
            return newInstance(null)
        }

        fun newInstance(issue_id: String?): CreateFragment {
            val fragment = CreateFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_KEY_OF_FRAGMENT, NEED_TO_SHOW_CREATE_FRAGMENT)
            if (issue_id != null) {
                bundle.putString(ISSUE_ID, issue_id)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var viewModel: IssueViewModel
    private var subs: MutableList<Disposable?> = mutableListOf()

    private lateinit var titleTextView: EditText
    private lateinit var descriptionTextView: EditText
    private lateinit var dateView: TextView
    private lateinit var priorityView: AppCompatSpinner
    private lateinit var userSelect: AppCompatSpinner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.create_fragment, container, false)

        titleTextView = view.findViewById(R.id.issue_title)
        descriptionTextView = view.findViewById(R.id.issue_description)
        dateView = view.findViewById(R.id.date_choose)
        priorityView = view.findViewById(R.id.priority_choose)
        userSelect = view.findViewById(R.id.user_choose)

        return view
    }

    private fun initObservables() {
        subs.add(RxTextView.textChanges(titleTextView)
                .subscribeOn(Schedulers.io())
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { viewModel.title = it.toString() })
        subs.add(RxTextView.textChanges(descriptionTextView)
                .subscribeOn(Schedulers.io())
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { viewModel.description = it.toString() })
        subs.add(RxTextView.textChanges(dateView)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribe { viewModel.expirationDate = it.toString() })
        subs.add(RxAdapterView.itemSelections(priorityView)
                .subscribeOn(AndroidSchedulers.mainThread())//Really, I don't know why it's subscriber on MainThread, but it is
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it != -1) {//DEBUG FIX, lol
                        viewModel.priority = Priority.fromOrdinal(it).toString()
                    }
                })
        subs.add(RxAdapterView.itemSelections(userSelect)//TODO now you 
need go to this screen few times, before new users will be added. Do it 
NORMAL!
                .subscribeOn(AndroidSchedulers.mainThread())//Really, I don't know why it's subscriber on MainThread, but it is
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it != -1) {//DEBUG FIX, lol
                        viewModel.userName = userSelect.selectedItem.toString()
                    }
                })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(IssueViewModel::class.java)
        checkEditAndSetIfItNeed()

        initObservables()
        addOnClickListeners()
    }

    private fun checkEditAndSetIfItNeed() {
        if (arguments != null) {
            val extraId = arguments?.getString(ISSUE_ID)
            if (extraId != null) {
                subs.add(IssuesRepository.getLocalIssueById(extraId)
                        .observeOn(AndroidSchedulers.mainThread())
                        ?.subscribeOn(Schedulers.io())
                        ?.subscribe {
                            viewModel.init(it)

                            titleTextView.setText(it.title)
                            descriptionTextView.setText(it.description)
                            priorityView.setSelection(it.priority.ordinal)
                            dateView.text = DATE_FORMAT.format(it.expirationDate)
                        })
            }

        }
    }

    private fun addOnClickListeners() {
        dateView.setOnClickListener {
            DialogHelper.showCalendarDialog(activity!!, onResult = { y: Int, m: Int, d: Int ->
                dateView.text = DATE_FORMAT.format(parseDate("$d." + (m + 1) + ".$y"))
            })
        }
        priorityView.adapter = SpinnerAdapter(context!!)
        initUsers()
    }

    private fun initUsers() {
        subs.add(Observable.just("", "", "", "", "")//TODO replace with Single, remove multiple items
                .observeOn(Schedulers.io())
                .debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())//fixme? SQL hack
                .subscribe({
                    userSelect.adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, IssuesRepository.getAllUsers())
                }, {
                    Log.e("SQL, stop it", it.message, it)
                }))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.save, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.save -> {
                context?.let {
                    IssuesRepository.createIssue(viewModel)
                }
                activity?.finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        for (subscription in subs) {
            subscription?.dispose()
        }
        priorityView.adapter = null
    }
}
