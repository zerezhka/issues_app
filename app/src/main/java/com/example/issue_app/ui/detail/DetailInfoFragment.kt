package com.example.issue_app.ui.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.TextView
import com.example.issue_app.R
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.repository.DATE_FORMAT
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.ui.EXTRA_KEY_OF_FRAGMENT
import com.example.issue_app.ui.MainActivity
import com.example.issue_app.ui.create.ISSUE_ID
import com.example.issue_app.ui.create.NEED_TO_SHOW_CREATE_FRAGMENT
import com.example.issue_app.utils.DialogHelper.Companion.showDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

const val NEED_TO_SHOW_DETAILS_FRAGMENT = "show_me_more_info!"

class DetailInfoFragment : Fragment() {
    companion object {
        fun newInstance(): DetailInfoFragment {
            return newInstance(null)
        }

        fun newInstance(issue_id: String?): DetailInfoFragment {
            val fragment = DetailInfoFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_KEY_OF_FRAGMENT, NEED_TO_SHOW_CREATE_FRAGMENT)
            if (issue_id != null) {
                bundle.putString(ISSUE_ID, issue_id)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var title: TextView
    lateinit var description: TextView
    lateinit var creationDate: TextView
    lateinit var expiredDate: TextView
    lateinit var priority: TextView
    lateinit var assignee: TextView
    lateinit var creatorId: TextView
    lateinit var close: Button


    private var subs: MutableList<Disposable> = mutableListOf()
    private lateinit var issue: Issue

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.detail_info_fragment, container, false)
        setHasOptionsMenu(true)
        findViews(view)
        initListeners()
        initDates()

        return view
    }

    private fun initDates() {
        subs.add(IssuesRepository.getLocalIssueById(arguments?.getString(ISSUE_ID)!!)
                .subscribe({
            creationDate.text = DATE_FORMAT.format(it.creationDate)
            expiredDate.text = DATE_FORMAT.format(it.expirationDate)
                }, {
                    Log.e("SQL", it.message, it)
                })
        )
    }

    private fun initListeners() {
        close.setOnClickListener {
            showDialog(context = context!!,
                    title = if (issue.closed) {
                        R.string.are_you_reopen_task
                    } else {
                        R.string.are_you_finish_task
                    },
                    yesText = R.string.yes,
                    noText = R.string.no,
                    onYesClick = {
                        issue.closed = !issue.closed
                        IssuesRepository.save(issue)
                        activity?.finish()
                    })
        }
        subs.add(IssuesRepository.getLocalIssueById(arguments?.getString(ISSUE_ID)!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe {
                    issue = it
                    title.text = it.title
                    description.text = it.description
                    priority.text = it.priority.toString()
                    if (issue.closed) {
                        close.setText(R.string.reopen)
                    }/* else {
                        close.setText(R.string.mark_closed)
                    }*/
                })
        subs.add(IssuesRepository.getLocalIssueById(arguments?.getString(ISSUE_ID)!!)
                .map {
                    mapOf(Pair("creator", IssuesRepository.getUser(it.creatorId)?.name),
                            Pair("assignee", IssuesRepository.getUser(it.assigneeId)?.name))
                }
                .debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe {
                    creatorId.text = it["creator"]
                    assignee.text = it["assignee"]
                })
    }

    private fun findViews(view: View) {
        title = view.findViewById(R.id.issue_title)
        description = view.findViewById(R.id.issue_description)
        priority = view.findViewById(R.id.issue_priority)
        assignee = view.findViewById(R.id.assignee)
        creatorId = view.findViewById(R.id.creator_id)
        creationDate = view.findViewById(R.id.creation_date)
        expiredDate = view.findViewById(R.id.expiration_date)
        close = view.findViewById(R.id.close)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.details_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_edit -> {
                issue.id.let {
                    MainActivity.startNewInstance(activity!!, NEED_TO_SHOW_CREATE_FRAGMENT, it)
                    activity?.finish()
                    return true
                }
            }
            R.id.menu_delete -> {
                showDialog(activity!!, getString(R.string.are_you_sure), "Удалить", "Отмена", {
                    activity?.finish()
                    issue.id.let { IssuesRepository.delete(it) }
                })
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        for (sub in subs) {
            sub.dispose()
        }
    }
}