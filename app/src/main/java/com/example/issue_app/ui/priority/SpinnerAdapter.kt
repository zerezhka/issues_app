package com.example.issue_app.ui.priority

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.issue_app.R
import com.example.issue_app.data.issue.Priority

class SpinnerAdapter(context: Context) : ArrayAdapter<CharSequence>(context, R.layout.item_spinner, context.resources?.getStringArray(R.array.priorities)) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = super.getView(position, convertView, parent)
        initIcon(view, position)
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = super.getDropDownView(position, convertView, parent)
        initIcon(view, position)
        return view
    }

    private fun initIcon(view: View?, position: Int) {
        if (view is TextView) {
            val img = context.resources.getDrawable(Priority.fromOrdinal(position).getRes())
            view.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
            view.gravity = Gravity.CENTER_VERTICAL
            view.compoundDrawablePadding = 15
            view.setPadding(15, 15, 0, 0)
        }
    }
}