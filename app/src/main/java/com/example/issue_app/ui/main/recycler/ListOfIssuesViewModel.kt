package com.example.issue_app.ui.main.recycler

import android.arch.lifecycle.ViewModel
import com.example.issue_app.data.issue.Issue

class ListOfIssuesViewModel : ViewModel() {
    var issues: List<Issue>? = null
    var recyclerAdapter: RecyclerAdapter? = null


    fun init(issues: List<Issue>, adapter: RecyclerAdapter) {
        this.issues = issues
        recyclerAdapter = adapter
        recyclerAdapter?.items = issues
        recyclerAdapter?.items = recyclerAdapter?.items?.sortedWith(compareBy(Issue::expirationDate, { it.priority }))//Прекл, можно написать 2 вариантами)
        recyclerAdapter?.notifyDataSetChanged()
    }

    /**
     * На всякий случай, чтобы память не утекала
     */
    override fun onCleared() {
        super.onCleared()
        issues = null
        recyclerAdapter = null
    }
}
