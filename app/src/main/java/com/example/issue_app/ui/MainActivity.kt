package com.example.issue_app.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import com.example.issue_app.R
import com.example.issue_app.ui.create.CreateFragment
import com.example.issue_app.ui.create.ISSUE_ID
import com.example.issue_app.ui.create.NEED_TO_SHOW_CREATE_FRAGMENT
import com.example.issue_app.ui.detail.DetailInfoFragment
import com.example.issue_app.ui.detail.NEED_TO_SHOW_DETAILS_FRAGMENT
import com.example.issue_app.ui.main.MainFragment
import io.fabric.sdk.android.Fabric
import io.reactivex.disposables.Disposable


const val EXTRA_KEY_OF_FRAGMENT = "extra_bundle_fragment_key"

class MainActivity : AppCompatActivity() {
    companion object {
        fun startNewInstance(context: Context, fragment_id: String, issueId: String, finishActivity: Boolean) {
            if (context is Activity && finishActivity) {
                context.finish()
            }
            context.startActivity(getStartIntent(context, fragment_id, issueId))
        }

        fun startNewInstance(context: Context, fragment_id: String, issueId: String) {
            startNewInstance(context, fragment_id, issueId, false)
        }

        fun startNewInstance(context: Context) {
            context.startActivity(getStartIntent(context))
        }

        fun getStartIntent(context: Context, fragment_id: String, issueId: String): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(EXTRA_KEY_OF_FRAGMENT, fragment_id)
            intent.putExtra(ISSUE_ID, issueId)
            return intent
        }

        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            return intent
        }
    }

    private var subs: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.main_activity)


        supportActionBar?.elevation = 0f
//        val issues = parseJsonFromRaw(resources, R.raw.list_of_issues)

        if (savedInstanceState == null) {
            val fragment =
                    when (intent?.extras?.getString(EXTRA_KEY_OF_FRAGMENT)) {
                        NEED_TO_SHOW_CREATE_FRAGMENT -> CreateFragment.newInstance(intent?.extras?.getString(ISSUE_ID))
                        NEED_TO_SHOW_DETAILS_FRAGMENT -> DetailInfoFragment.newInstance(intent?.extras?.getString(ISSUE_ID))
//                        NEED_TO_SHOW_SHARE_FRAGMENT -> ShareFragment.newInstance()
//                        NEED_TO_SHOW_EDIT_FRAGMENT -> EditFragment.newInstance()
                        else -> MainFragment.newInstance()
                    }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commitNow()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        subs?.dispose()
        subs = null
    }
}
