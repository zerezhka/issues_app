package com.example.issue_app.ui.create

import android.arch.lifecycle.ViewModel
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.repository.DATE_FORMAT

class IssueViewModel : ViewModel() {

    fun init(issue: Issue) {
        issueId = issue.id
        this.title = issue.title
        this.description = issue.description
        this.creationDate = DATE_FORMAT.format(issue.creationDate)
        this.expirationDate = DATE_FORMAT.format(issue.expirationDate)
        this.priority = issue.priority.name
        this.userName = issue.assigneeId//todo переделать на user.Name, instead of user.Id
        this.closed = issue.closed
    }

    var issueId: String? = null
    lateinit var title: String
    lateinit var description: String
    lateinit var creationDate: String
    lateinit var expirationDate: String
    lateinit var priority: String
    lateinit var userName: String
    var closed: Boolean = false

}