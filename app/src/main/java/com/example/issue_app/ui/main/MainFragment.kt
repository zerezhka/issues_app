package com.example.issue_app.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.*
import com.example.issue_app.R
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.ui.EXTRA_KEY_OF_FRAGMENT
import com.example.issue_app.ui.MainActivity
import com.example.issue_app.ui.create.NEED_TO_SHOW_CREATE_FRAGMENT
import com.example.issue_app.ui.login.LoginActivity
import com.example.issue_app.ui.main.viewpager.InPagerFragment
import com.example.issue_app.ui.main.viewpager.MainPagerAdapter
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


private const val OPENED = "Открытые"
private const val CLOSED = "Завершенные"

class MainFragment : Fragment() {

    private var subs: MutableList<Disposable> = mutableListOf()

    companion object {
        fun newInstance(): MainFragment = MainFragment()
    }

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var adapter: MainPagerAdapter
    private lateinit var floatingActionButtonAdd: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)

        setHasOptionsMenu(true)
        findViews(view)
        initViewPager()
        initCreateClick()
        syncIssuesWithFirebaseDb()
        syncUsersWithFirebaseDb()

        return view
    }

    private fun syncUsersWithFirebaseDb() {
        IssuesRepository.getUsersFromFirebaseDb()
    }

    private fun syncIssuesWithFirebaseDb() {
        IssuesRepository.getIssuesFromServer()
        subs.add(Observable.just("", "", "", "")//TODO remove multiple params
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .debounce(2, TimeUnit.SECONDS)//fixme? hack for SQL
                .subscribe({
                    val issues = IssuesRepository.getAllIssues()
                    IssuesRepository.setFirebaseIssues(issues!!)
                }, {
                    Log.e("SQL", it.message, it)
                }))
    }

    private fun findViews(view: View) {
        viewPager = view.findViewById(R.id.view_pager)
        tabLayout = view.findViewById(R.id.tabLayout)
        floatingActionButtonAdd = view.findViewById(R.id.floatingActionButtonAdd)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.logout, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.logout -> {
                FirebaseAuth.getInstance().signOut()
                val loginActivity = Intent(context, LoginActivity::class.java)
                activity?.finish()
                startActivity(loginActivity)
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }
    private fun initCreateClick() {
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra(EXTRA_KEY_OF_FRAGMENT, NEED_TO_SHOW_CREATE_FRAGMENT)
        floatingActionButtonAdd.setOnClickListener {
            startActivity(intent)
        }
    }

    private fun initViewPager() {
        fragmentManager.let { adapter = MainPagerAdapter(fragmentManager!!) }
        adapter.addFragment(InPagerFragment.newInstance(false), OPENED)
        adapter.addFragment(InPagerFragment.newInstance(true), CLOSED)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewPager.adapter = null
        for (sub in subs) {
            sub.dispose()
        }
        subs.clear()
    }
}