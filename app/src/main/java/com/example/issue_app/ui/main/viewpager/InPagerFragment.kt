package com.example.issue_app.ui.main.viewpager

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.issue_app.R
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.ui.main.recycler.ListOfIssuesViewModel
import com.example.issue_app.ui.main.recycler.RecyclerAdapter
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

private const val EXTRA_COMPLETED_TASKS = "extra_completed_tasks_fragment"

class InPagerFragment : Fragment() {

    companion object {
        fun newInstance(completed: Boolean): InPagerFragment {
            val fragment = InPagerFragment()
            val bundle = Bundle()
            bundle.putBoolean(EXTRA_COMPLETED_TASKS, completed)
            fragment.arguments = bundle
            return fragment
        }
    }

    private var subs: Disposable? = null

    private lateinit var viewModel: ListOfIssuesViewModel
    private var recycler: RecyclerView? = null
    private var showCompleted: Boolean = false
    private fun asyncRxGetFromDB(): Disposable? {
        return getDbIssues()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    viewModel.init(
                            it,
                            (recycler?.adapter as RecyclerAdapter)
                    )
                })
    }

    private fun getDbIssues(): Flowable<List<Issue>>? {
        return if (showCompleted) {
            IssuesRepository.getCompletedIssues()
        } else {
            IssuesRepository.getNotCompletedIssues()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.pager_content_layout, container, false)
        recycler = view.findViewById(R.id.recycler)
        showCompleted = arguments?.getBoolean(EXTRA_COMPLETED_TASKS)!!
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListOfIssuesViewModel::class.java)
        if (viewModel.recyclerAdapter != null) {
            recycler?.adapter = viewModel.recyclerAdapter
        } else {
            recycler?.adapter = RecyclerAdapter()
        }

        subs = asyncRxGetFromDB()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        subs?.dispose()
        subs = null
        recycler?.adapter = null //абсолютно точно без этого будут утечки памяти [see](https://stackoverflow.com/questions/35520946/leak-canary-recyclerview-leaking-madapter)
    }

}
