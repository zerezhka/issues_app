package com.example.issue_app.ui.main.recycler

import android.content.Intent
import android.graphics.Paint
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.issue_app.R
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.ui.EXTRA_KEY_OF_FRAGMENT
import com.example.issue_app.ui.MainActivity
import com.example.issue_app.ui.create.ISSUE_ID
import com.example.issue_app.ui.create.NEED_TO_SHOW_CREATE_FRAGMENT
import com.example.issue_app.ui.detail.NEED_TO_SHOW_DETAILS_FRAGMENT
import com.example.issue_app.utils.DialogHelper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.IssuesViewHolder>() {
    var items: List<Issue>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssuesViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_issue_item_view, parent, false)
        val vh = IssuesViewHolder(mView)
        vh.icon = mView.findViewById(R.id.icon)
        vh.title = mView.findViewById(R.id.title)
        vh.description = mView.findViewById(R.id.description)
        return vh
    }

    override fun getItemCount(): Int {
        return if (items == null) {
            0
        } else {
            items?.size!!
        }
    }

    override fun onBindViewHolder(holder: IssuesViewHolder, position: Int) {
        val curItem = items?.get(position)
        curItem?.priority?.getRes()?.let { holder.icon?.setImageResource(it) }
        holder.title?.text = items?.get(position)?.title
        holder.description?.text = items?.get(position)?.description

        if (curItem?.closed!!) {
            holder.title?.paintFlags = holder.title?.paintFlags!! or Paint.STRIKE_THRU_TEXT_FLAG
            holder.description?.paintFlags = holder.description?.paintFlags!! or Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            holder.title?.paintFlags = holder.title?.paintFlags!! and Paint.STRIKE_THRU_TEXT_FLAG
            holder.description?.paintFlags = holder.description?.paintFlags!! and Paint.STRIKE_THRU_TEXT_FLAG
        }
        setUpClickListener(holder, curItem)
        setUpOnLongClick(holder, curItem)
    }

    private fun setUpClickListener(holder: IssuesViewHolder, curItem: Issue?) {
        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            MainActivity.startNewInstance(context, NEED_TO_SHOW_DETAILS_FRAGMENT, curItem?.id!!)
        }
    }

    private fun setUpOnLongClick(holder: IssuesViewHolder, curItem: Issue?) {
        val popup = PopupMenu(holder.itemView.context, holder.itemView)
        popup.inflate(R.menu.on_list_long_click)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.edit -> {
                    Log.d("EDIT", holder.title?.text.toString())
                    val intent = Intent(holder.itemView.context, MainActivity::class.java)
                    intent.putExtra(EXTRA_KEY_OF_FRAGMENT, NEED_TO_SHOW_CREATE_FRAGMENT)
                    intent.putExtra(ISSUE_ID, curItem?.id)
                    holder.itemView.context.startActivity(intent)

                    true
                }
                R.id.delete -> {
                    Log.d("DELETE", holder.title?.text.toString())
                    Observable.just(curItem?.id)//TODO replace with Single
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                DialogHelper.Companion.showDialog(context = holder.itemView.context,
                                        title = R.string.are_you_sure, yesText = R.string.yes, noText = R.string.no,
                                        onYesClick = {
                                            it?.let { id -> IssuesRepository.delete(id) }
                                        })
                            }
                    true
                }
                else -> {
                    false
                }
            }
        }
        holder.itemView.setOnLongClickListener {
            popup.show()
            true
        }
    }


    class IssuesViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var icon: ImageView? = null
        var title: TextView? = null
        var description: TextView? = null
    }

}

