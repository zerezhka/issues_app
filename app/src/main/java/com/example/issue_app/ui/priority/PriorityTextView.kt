package com.example.issue_app.ui.priority

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import com.example.issue_app.data.issue.Priority

class PriorityTextView : TextView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun setText(text: CharSequence?, type: BufferType?) {
        super.setText(text, type)
        if (!TextUtils.isEmpty(text)) {
            initIcon()
        }
    }

    private fun initIcon() {
        val img = context.resources.getDrawable(Priority.valueOf(text.toString()).getRes())
        this.pivotX = 0f
        this.pivotY = 0f
        this.scaleX = 0.5f
        this.scaleY = 0.5f
        this.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null)
        this.compoundDrawablePadding = 5
        this.gravity = Gravity.CENTER_VERTICAL
        this.setPadding(15, 0, 0, 0)
    }
}