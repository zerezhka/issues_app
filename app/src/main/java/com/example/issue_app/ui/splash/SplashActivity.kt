package com.example.issue_app.ui.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.issue_app.R
import com.example.issue_app.repository.IssuesRepository
import com.example.issue_app.ui.MainActivity
import com.example.issue_app.ui.login.LoginActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SplashActivity : AppCompatActivity() {
    lateinit var subs: Disposable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        subs = Observable.just("")//TODO replace with Single
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val cls =
                            if (IssuesRepository.getCurrentUser() != null) {
                                MainActivity::class.java
                            } else {
                                LoginActivity::class.java
                            }
                    val intent = Intent(this, cls)
                    finish()
                    startActivity(intent)
                }
    }

    override fun onDestroy() {
        super.onDestroy()
        subs.dispose()
    }
}
