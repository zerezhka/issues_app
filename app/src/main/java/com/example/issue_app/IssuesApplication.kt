package com.example.issue_app

import android.app.Application
import com.example.issue_app.db.InnerDB
import com.example.issue_app.repository.IssuesRepository

class IssuesApplication : Application() {
    var db: InnerDB? = null

    override fun onCreate() {
        super.onCreate()
        db = InnerDB.getInstance(this)
        IssuesRepository.init(db!!)
    }

    override fun onTerminate() {
        super.onTerminate()
        IssuesRepository.onDestroy()
    }
}