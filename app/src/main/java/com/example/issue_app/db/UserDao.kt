package com.example.issue_app.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.example.issue_app.data.user.User

@Dao
interface UserDao {

    @Insert(onConflict = REPLACE)
    fun setUser(user: User)

    @Query("SELECT * FROM users WHERE id = :id")
    fun getUser(id: String): User?

    @Query("SELECT * FROM users WHERE is_current = 1")
    fun getCurrentUser(): User?

    @Query("SELECT * FROM users")
    fun getAllUsers(): List<User>

    @Query("SELECT * FROM users WHERE user_name = :name")
    fun getUserByName(name: String): User

}