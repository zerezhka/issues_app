package com.example.issue_app.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.example.issue_app.data.issue.Issue
import io.reactivex.Flowable

@Dao
interface IssuesDao {
    @Insert(onConflict = REPLACE)
    fun save(issue: Issue)

    @Query("SELECT * FROM issues WHERE id = :id")
    fun getById(id: String): Flowable<Issue>

    @Query("SELECT * FROM issues")
    fun getAll(): Flowable<List<Issue>>

    @Query("DELETE FROM issues")
    fun clear()

    @Query("DELETE FROM issues WHERE id=:id")
    fun delete(id: String)

    @Query("SELECT * FROM issues WHERE closed = 1")
    fun getCompletedIssues(): Flowable<List<Issue>>

    @Query("SELECT * FROM issues WHERE closed = 0")
    fun getNotCompletedIssues(): Flowable<List<Issue>>


    @Query("UPDATE issues SET closed=1 WHERE id=:id")
    fun complete(id: String)

    @Query("UPDATE issues SET closed=0 WHERE id=:id")
    fun reopen(id: String)
}