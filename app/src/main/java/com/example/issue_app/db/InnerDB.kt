package com.example.issue_app.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.data.user.User

@Database(entities = [Issue::class, User::class], version = 9, exportSchema = false)
abstract class InnerDB : RoomDatabase() {
    abstract fun getIssuesDao(): IssuesDao
    abstract fun getUserDao(): UserDao

    /**
     * Singleton
     */
    companion object {
        private var INSTANCE: InnerDB? = null

        fun getInstance(context: Context): InnerDB? {
            if (INSTANCE == null) {
                synchronized(InnerDB::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            InnerDB::class.java, "issues.db")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun getInstance(): InnerDB? {
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
