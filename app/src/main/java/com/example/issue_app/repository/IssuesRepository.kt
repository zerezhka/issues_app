package com.example.issue_app.repository

import android.util.Log
import com.example.issue_app.data.issue.Issue
import com.example.issue_app.data.issue.Priority
import com.example.issue_app.data.user.User
import com.example.issue_app.db.InnerDB
import com.example.issue_app.ui.create.IssueViewModel
import com.example.issue_app.utils.saveIssueToFirebaseDb
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


val DATE_FORMAT = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

class IssuesRepository {
    companion object {
        private lateinit var DB: InnerDB
        private var subs: MutableList<Disposable> = mutableListOf()

        fun init(db: InnerDB) {
            DB = db
        }

        fun onDestroy() {
            subs.clear()
            DB.close()
        }

        fun getIssuesFromServer() {
            subs.add(RxFirebaseDatabase.observeMultipleSingleValueEvent(FirebaseDatabase.getInstance().getReference("issues")/*, DataSnapshotMapper.mapOf(HashMap<String, HashMap<String, String>>::class.java)*/)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .map { it.value as HashMap<String, HashMap<String, String>> }
                    .flatMapIterable { it -> it.entries }
                    .subscribe {
                        val issue = Issue()
                        val issueAsHashMap = it.value
                        issue.id = it.key
                        issue.creatorId = issueAsHashMap["creatorId"] as String
                        issue.assigneeId = issueAsHashMap["assigneeId"] as String
                        issue.creationDate.time = issueAsHashMap["creationDate"] as Long
                        issue.expirationDate.time = issueAsHashMap["expirationDate"] as Long
                        issue.title = issueAsHashMap["title"] as String
                        issue.description = issueAsHashMap["description"] as String
                        issue.closed = issueAsHashMap["closed"] as Boolean
                        issue.priority = Priority.valueOf(issueAsHashMap["priority"] as String)

                        DB.getIssuesDao().save(issue)
                    })
        }

        fun getLocalIssueById(id: String): Flowable<Issue> {
            return DB.getIssuesDao().getById(id)
        }

        fun createIssue(viewModel: IssueViewModel) {
            val id = when (viewModel.issueId) {
                null -> UUID.randomUUID().toString()
                else -> viewModel.issueId
            }
            subs.add(Observable.just(id)//TODO replace with Single
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe {
                        val issue = Issue(creatorId = DB.getUserDao().getCurrentUser()?.id!!,
                                assigneeId = DB.getUserDao().getUserByName(viewModel.userName).id,
                                creationDate = Date(),//static fields
                                expirationDate = parseDate(viewModel.expirationDate),
                                title = viewModel.title,
                                description = viewModel.description,
                                priority = Priority.valueOf(viewModel.priority),
                                closed = viewModel.closed,//user set fields
                                id = it!!)
                        saveIssueToFirebaseDb(issue)
                        DB.getIssuesDao().save(issue)
                    })
        }

        fun delete(id: String) {
            subs.add(Observable.just(id)//TODO replace with Single
                    .observeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe {
                        DB.getIssuesDao().delete(it)
                        FirebaseDatabase.getInstance().getReference("issues/$it").removeValue()
                    })
        }

        fun save(issue: Issue) {
            subs.add(Observable.just(issue)//TODO replace with Single
                    .observeOn(Schedulers.io())
                    .subscribe {
                        DB.getIssuesDao().save(issue)
                        saveIssueToFirebaseDb(issue = issue)
                    })
        }

        fun createCurrentUser(user: FirebaseUser?) {
            subs.add(Observable.just(user)//TODO replace with Single
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe {
                        val user = User(id = it?.uid.toString(), name = it?.displayName!!, isCurrent = true)
                        DB.getUserDao().setUser(user)
                    }
            )
        }

        fun getCurrentUser(): User? {
            return DB.getUserDao().getCurrentUser()
        }

        fun getUser(userId: String): User? {
            return DB.getUserDao().getUser(userId)
        }

        fun getAllIssues(): List<Issue>? {
            return ArrayList(DB.getIssuesDao().getAll().blockingSingle())
        }

        fun getCompletedIssues(): Flowable<List<Issue>>? {
            return DB.getIssuesDao().getCompletedIssues()
        }

        fun getNotCompletedIssues(): Flowable<List<Issue>>? {
            return DB.getIssuesDao().getNotCompletedIssues()
        }

        fun getUsersFromFirebaseDb() {
            val usersQuery: Query = com.example.issue_app.utils.db.reference.child("users")
            usersQuery.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Log.e("FIREBASE", p0.message, p0.toException())
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {//TODO add more checks for realtimeDatabase data to resolve potentially crashes
                    val firebaseId = FirebaseAuth.getInstance().currentUser?.uid
                    if (dataSnapshot.value is HashMap<*, *>) {
                        val rawUsers = (dataSnapshot.value as HashMap<String, HashMap<String, String>>)
                        val users = mutableListOf<User>()
                        for (rawUser in rawUsers) {
                            users.add(User(rawUser.key, rawUser.value["name"]!!, rawUser.key == firebaseId))
                        }
                        subs.add(Observable.just("")//TODO replace with Single
                                .observeOn(Schedulers.io())
                                .debounce(300, TimeUnit.MILLISECONDS, Schedulers.io())//fixme? hack =/
                                .subscribe {
                                    setUsers(users)
                                })
                    }
                }
            })

        }

        fun getAllUsers(): List<User> {
            getUsersFromFirebaseDb()
            return DB.getUserDao().getAllUsers()
        }

        fun setUser(user: User) {
            DB.getUserDao().setUser(user)
        }

        fun setUsers(users: List<User>) {
            for (user in users) {
                DB.getUserDao().setUser(user)
            }
        }

        fun setFirebaseIssues(issues: List<Issue>) {
            for (issue in issues) {
                saveIssueToFirebaseDb(issue)
            }
        }

    }
}

fun parseDate(stringDate: String): Date {
    return try {
        DATE_FORMAT.parse(stringDate)
    } catch (ex: Exception) {
        ex.printStackTrace()
        Date()
    }
}
