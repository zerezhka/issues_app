package com.example.issue_app.data.room.converters

import android.arch.persistence.room.TypeConverter
import com.example.issue_app.data.issue.Priority

class EnumConverter {
    @TypeConverter
    fun fromEnum(enum: Priority): String {
        return enum.name
    }

    @TypeConverter
    fun toEnum(string: String): Priority {
        return Priority.valueOf(string)
    }
}