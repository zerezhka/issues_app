package com.example.issue_app.data.user

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
data class User(@PrimaryKey(autoGenerate = false)
                @ColumnInfo(name = "id") var id: String,
                @ColumnInfo(name = "user_name") var name: String,
                @ColumnInfo(name = "is_current") var isCurrent: Boolean) {
    companion object {
        val LOCAL_USER: User = User("0", "Вы", true)
    }

    override fun toString(): String {
        return name
    }
}
