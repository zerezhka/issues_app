package com.example.issue_app.data.issue

import android.arch.persistence.room.*
import com.example.issue_app.data.room.converters.BooleanConverter
import com.example.issue_app.data.room.converters.DateConverter
import com.example.issue_app.data.room.converters.EnumConverter
import com.example.issue_app.data.user.User
import java.util.*


@Entity(tableName = "issues")
@TypeConverters(EnumConverter::class, DateConverter::class, BooleanConverter::class)
data class Issue(@PrimaryKey(autoGenerate = false) @ColumnInfo(name = "id") var id: String,

                 @ForeignKey(entity = User::class,
                         parentColumns = ["id"],
                         childColumns = ["creatorId"],
                         onDelete = ForeignKey.CASCADE)
                 @ColumnInfo(name = "creatorId") var creatorId: String,

                 @ForeignKey(entity = User::class,
                         parentColumns = ["id"],
                         childColumns = ["assignee_id"],
                         onDelete = ForeignKey.CASCADE)
                 @ColumnInfo(name = "assignee_id") var assigneeId: String,
                 @ColumnInfo(name = "creation_date") var creationDate: Date,
                 @ColumnInfo(name = "end_date") var expirationDate: Date,
                 @ColumnInfo(name = "title") var title: String,
                 @ColumnInfo(name = "description") var description: String,
                 @ColumnInfo(name = "priority") var priority: Priority,
                 @ColumnInfo(name = "closed") var closed: Boolean) {

    @Ignore
    constructor() : this("local_id", User.LOCAL_USER.id, User.LOCAL_USER.id, Date(), Date(), "", "", Priority.MEDIUM, false)

    override fun toString(): String {
        return "Issue(id='$id', creatorId='$creatorId', assigneeId='$assigneeId', creationDate=$creationDate, expirationDate=$expirationDate, title='$title', description='$description', priority=$priority, closed=$closed)"
    }

}