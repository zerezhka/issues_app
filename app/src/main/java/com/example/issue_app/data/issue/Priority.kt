package com.example.issue_app.data.issue

import android.support.annotation.DrawableRes
import com.example.issue_app.R

enum class Priority(@DrawableRes private val resId: Int) {
    TRIVIAL(R.drawable.ic_circle_trivial),

    LOWEST(R.drawable.ic_circle_lowest),
    LOW(R.drawable.ic_circle_low),

    MINOR(R.drawable.ic_circle_minor),
    MEDIUM(R.drawable.ic_circle_medium),
    MAJOR(R.drawable.ic_circle_major),

    HIGH(R.drawable.ic_circle_high),
    HIGHEST(R.drawable.ic_circle_highest),

    CRITICAL(R.drawable.ic_circle_critical),

    BLOCKER(R.drawable.ic_circle_blocker);

    @DrawableRes
    fun getRes(): Int {
        return resId
    }

    companion object {
        fun Priority.ordinal(): Int {
            return ordinal
        }

        fun fromOrdinal(int: Int): Priority {
            for (v in values()) {
                if (v.ordinal == int) return v
            }
            throw Exception("not exist")
        }
    }
}