package com.example.issue_app.data.room.converters

import android.arch.persistence.room.TypeConverter

class BooleanConverter {
    @TypeConverter
    fun fromBoolean(b: Boolean): Int {
        return if (b) {
            1
        } else {
            0
        }
    }

    @TypeConverter
    fun toBoolean(int: Int): Boolean {
        return int == 1
    }
}