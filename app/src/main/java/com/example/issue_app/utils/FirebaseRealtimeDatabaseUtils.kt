package com.example.issue_app.utils

import com.example.issue_app.data.issue.Issue
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase


val db = FirebaseDatabase.getInstance()

fun saveUserToFirebaseDb(user: FirebaseUser?) {
    val usersAsHashMap = HashMap<String, String>()
//    usersAsHashMap["id"] = user?.uid!!
    usersAsHashMap["name"] = user?.displayName!!
    db.getReference("users/" + user.uid).setValue(usersAsHashMap)
}

fun saveIssueToFirebaseDb(issue: Issue) {
    val issueAsHashMap = HashMap<String, Any>()
    issueAsHashMap["creatorId"] = issue.creatorId
    issueAsHashMap["assigneeId"] = issue.assigneeId
    issueAsHashMap["creationDate"] = issue.creationDate.time
    issueAsHashMap["expirationDate"] = issue.expirationDate.time
    issueAsHashMap["title"] = issue.title
    issueAsHashMap["description"] = issue.description
    issueAsHashMap["closed"] = issue.closed
    issueAsHashMap["priority"] = issue.priority.name

    val idRef = db.getReference("issues/" + issue.id)
    idRef.setValue(issueAsHashMap)
}
//TODO make some converter