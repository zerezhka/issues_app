package com.example.issue_app.utils

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.view.View
import java.util.*

class DialogHelper {
    companion object {

        fun showDialog(context: Context,
                       title: String,
                       yesText: String,
                       noText: String,
                       onYesClick: () -> Unit) {
            showDialog(context, title, yesText, noText, onYesClick, {})
        }

        fun showDialog(context: Context,
                       title: String,
                       yesText: String,
                       noText: String,
                       onYesClick: () -> Unit,
                       onNoClick: () -> Unit) {

            val dialogAlertDialog = AlertDialog.Builder(context)
                    .setTitle(title)
                    .setPositiveButton(yesText, { dialogInterface: DialogInterface, i: Int ->
                        onYesClick()
                        dialogInterface.dismiss()
                    })
                    .setNegativeButton(noText, { dialogInterface, i ->
                        onNoClick()
                        dialogInterface.cancel()
                    })
            dialogAlertDialog.create().show()
        }

        fun showDialog(context: Context,
                       @StringRes title: Int,
                       @StringRes yesText: Int,
                       @StringRes noText: Int,
                       onYesClick: () -> Unit) {
            showDialog(context, title, yesText, noText, onYesClick, {})
        }


        fun showDialog(context: Context,
                       @StringRes title: Int,
                       @StringRes yesText: Int,
                       @StringRes noText: Int,
                       onYesClick: () -> Unit,
                       onNoClick: () -> Unit) {

            val dialogAlertDialog = AlertDialog.Builder(context)
                    .setTitle(title)
                    .setPositiveButton(yesText, { dialogInterface: DialogInterface, i: Int ->
                        dialogInterface.dismiss()
                        onYesClick()
                    })
                    .setNegativeButton(noText, { dialogInterface, i ->
                        dialogInterface.cancel()
                        onNoClick()
                    })
            dialogAlertDialog.create().show()
        }

        fun showCalendarDialog(context: Context, onResult: (y: Int, m: Int, d: Int) -> Unit) {
            val now = Date()
            val dialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener({ _: View, y: Int, m: Int, d: Int ->
                onResult(y, m, d)
            }), now.year, now.month, now.day)
            dialog.datePicker.minDate = now.time//TODO добавить время
            dialog.show()
        }
    }
}